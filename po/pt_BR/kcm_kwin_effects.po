# Translation of kcm_kwin_effects.po to Brazilian Portuguese
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2019.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:00+0000\n"
"PO-Revision-Date: 2019-10-03 10:29-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 19.08.1\n"

#: package/contents/ui/Effect.qml:92
#, kde-format
msgid ""
"Author: %1\n"
"License: %2"
msgstr ""
"Autor: %1\n"
"Licença: %2"

#: package/contents/ui/Effect.qml:121
#, kde-format
msgctxt "@info:tooltip"
msgid "Show/Hide Video"
msgstr "Mostrar/Ocultar o vídeo"

#: package/contents/ui/Effect.qml:128
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure..."
msgstr "Configurar..."

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you configure desktop effects."
msgstr "Este módulo permite que você configure os efeitos da área de trabalho."

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Hint: To find out or configure how to activate an effect, look at the "
"effect's settings."
msgstr ""
"Dica: Para localizar ou configurar a ativação de um efeito, verifique as "
"configurações do efeito."

#: package/contents/ui/main.qml:47
#, kde-format
msgid "Configure Filter"
msgstr "Configurar o filtro"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Exclude unsupported effects"
msgstr "Excluir os efeitos sem suporte"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Exclude internal effects"
msgstr "Excluir os efeitos internos"

#: package/contents/ui/main.qml:125
#, kde-format
msgid "Get New Desktop Effects..."
msgstr "Baixar novos efeitos da área de trabalho..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Luiz Fernando Ranghetti, André Marcelo Alvarenga"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "elchevive@opensuse.org, alvarenga@kde.org"

#~ msgid "Desktop Effects"
#~ msgstr "Efeitos da área de trabalho"

#~ msgid "Vlad Zahorodnii"
#~ msgstr "Vlad Zahorodnii"

#~ msgid "Download New Desktop Effects"
#~ msgstr "Baixar novos efeitos da área de trabalho"
